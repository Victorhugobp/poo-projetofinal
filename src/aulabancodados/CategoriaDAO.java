/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aulabancodados;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author victo
 */
public class CategoriaDAO {
    public Connection conexaoTeste;

 
    public void teste(){
        

        System.out.println(Conexao.getConexao());
    }
    
    
        public void inserir(Categoria novaCategoria) {
        conexaoTeste = Conexao.getConexao();
        try {
            PreparedStatement nvfunc = this.conexaoTeste.prepareStatement("INSERT INTO categoria (descricao) VALUES (?)");
            nvfunc.setString(1, novaCategoria.getDescricaoCategoria());
            nvfunc.execute();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
     

    public void deletar(Categoria deletarCategoria){
        conexaoTeste = Conexao.getConexao();
        try{
            PreparedStatement nvfunc = this.conexaoTeste.prepareStatement("DELETE FROM categoria WHERE id = ?");
            nvfunc.setInt(1, deletarCategoria.getId_categoria());
            nvfunc.execute();
        }
        catch(SQLException ex){
            Logger.getLogger(ex.getMessage());
        }
    }
    public void update (Categoria updateCategoria){
        conexaoTeste = Conexao.getConexao();
        try{
            PreparedStatement nvfunc = this.conexaoTeste.prepareStatement("UPDATE categoria SET descricao = ? WHERE id = ?");
            nvfunc.setString(1, updateCategoria.getDescricaoCategoria());
            nvfunc.setInt(2, updateCategoria.getId_categoria());
            nvfunc.execute();
           
        }catch (SQLException ex){
            Logger.getLogger(ex.getMessage());
        }
    }
    public void inserirArray(ArrayList<Categoria>  Categoria) {
        try {
            for (Categoria ctg : Categoria) {
                this.inserir(ctg);
                this.update(ctg);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
    

 public ArrayList<Categoria> obterTodos() {
        conexaoTeste = Conexao.getConexao();
        try {
            
            PreparedStatement nvfunc = conexaoTeste.prepareStatement("SELECT * FROM categoria");
            ResultSet tst = nvfunc.executeQuery();
            
            ArrayList<Categoria> categorias = new ArrayList<>();
            
            while (tst.next()) {
                Categoria categoria =  new Categoria(tst.getInt("id"),tst.getString("descricao"));
                categorias.add(categoria);
            }
            
            return categorias;
        } catch (Exception ex) {
            System.out.println("Error Guardar: "+ex.getMessage());
        }
        return null;
    }
    
}
