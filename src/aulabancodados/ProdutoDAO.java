/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aulabancodados;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author victo
 */
public class ProdutoDAO {
    public Connection conexaoTeste;

 
    public void teste(){
        

        System.out.println(Conexao.getConexao());
    }
    
    
        public void inserir(Produto novoProduto) {
        conexaoTeste = Conexao.getConexao();
        try {
            PreparedStatement nvfunc = this.conexaoTeste.prepareStatement("INSERT INTO produtos (codigo, descricao) VALUES (?,?)");
            nvfunc.setString(1, novoProduto.getCodigo());
            nvfunc.setString(2, novoProduto.getDescricao());
            nvfunc.execute();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
     

    public void deletar(Produto deleteProduto){
        conexaoTeste = Conexao.getConexao();
        try{
            PreparedStatement nvfunc = this.conexaoTeste.prepareStatement("DELETE FROM produtos WHERE id = ?");
            nvfunc.setInt(1, deleteProduto.getId());
            nvfunc.execute();
        }
        catch(SQLException ex){
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void update (Produto updateProduto){
        conexaoTeste = Conexao.getConexao();
        try{
            PreparedStatement nvfunc = this.conexaoTeste.prepareStatement("UPDATE produtos SET codigo = ?, descricao = ? WHERE id = ?");
            nvfunc.setString(1, updateProduto.getCodigo());
            nvfunc.setString(2, updateProduto.getDescricao());
            nvfunc.setInt(3, updateProduto.getId());
            nvfunc.execute();
           
        }catch (SQLException ex){
            Logger.getLogger(ex.getMessage());
        }
    }
    public void inserirArray(ArrayList<Produto> produtos) {
        try {
            for (Produto prd : produtos) {
                this.inserir(prd);
                this.update(prd);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
    
    public ArrayList<Produto> obterTodos() {
        conexaoTeste = Conexao.getConexao();
        try {
            
            PreparedStatement nvfunc = conexaoTeste.prepareStatement("SELECT * FROM produtos");
            ResultSet tst = nvfunc.executeQuery();
            
            ArrayList<Produto> produtos = new ArrayList<>();
            
            while (tst.next()) {
                Produto produto =  new Produto(tst.getInt("id"),tst.getString("codigo"), tst.getString("descricao"));
                //produto.setId(tst.getInt("id"));
                produtos.add(produto);
            }
            
            return produtos;
        } catch (Exception ex) {
            System.out.println("Error Guardar: "+ex.getMessage());
        }
        return null;
    }
    

    

    
    
}
