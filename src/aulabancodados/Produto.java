/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aulabancodados;

/**
 *
 * @author victo
 */
public class Produto {
    private int id;
    private String codigo;
    private String descricao;
    
    public Produto(){
        
    }
    
    public Produto(int novoId, String novoCodigo, String novaDescricao){
        this.setId(novoId);
        this.setCodigo(novoCodigo);
        this.setDescricao(novaDescricao);
    }
    
    public void setId(int id){
        this.id = id;
    }
    public int getId(){
        return this.id;
    }
    
    public void setCodigo(String novoCodigo){
        this.codigo = novoCodigo;
    }
    public String getCodigo(){
        return this.codigo;
    }
    
    public void setDescricao(String novaDescricao){
        this.descricao = novaDescricao;
    }
    public String getDescricao(){
        return this.descricao;
    }
    



}
